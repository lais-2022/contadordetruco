import 'package:flutter/material.dart';

import 'home_page.dart';

void main(){
  runApp(
    const MaterialApp(
      title: "Contador de Truco",
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    )
  );
}