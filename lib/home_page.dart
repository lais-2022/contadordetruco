import 'package:contadordetruco/vitoria_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int placarNos = 0;
  int placarEles = 0;
  int vitoriaNos = 0;
  int vitoriaEles = 0;
  String grupoAtual = "";
  int pontoAtual = 0;

  @override
  void initState() {
    super.initState();
    carregarDados();
  }

  //não pode sofrer alteração mais(depois de valor recebido)
  // const int _placarNos = 0;
//valor unico(bild)
  Future<void> salvarDados() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('placarNos', placarNos);
    prefs.setInt('placarEles', placarEles);
    prefs.setInt('vitoriaNos', vitoriaNos);
    prefs.setInt('vitoriaEles', vitoriaEles);
    debugPrint('Salvar dados');
  }

  Future<void> carregarDados() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      placarNos = prefs.getInt('placarNos') ?? 0;
      placarEles = prefs.getInt('placarEles') ?? 0;
      vitoriaNos = prefs.getInt('vitoriaNos') ?? 0;
      vitoriaEles = prefs.getInt('vitoriaEles') ?? 0;
    });

    debugPrint('Carregar Dados....');
  }

  void aumentarPontos(String grupo, int pontos) {
    grupoAtual = grupo;
    pontoAtual = pontos;

    if (grupo == 'Nos') {
      setState(() {
        placarNos += pontos;

        if (placarNos >= 12) {
          vitoriaNos++;

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (contexto) => const VitoriaPage(
                grupo: "Nós",
              ),
            ),
          ).then((acao) {
            if (acao == "desfazer") {
              desfazer();
            } else if (acao == "novaPartida") {
              novaPartida();
            }
          });
        }
      });
      debugPrint(pontos.toString());
    }
    if (grupo == 'Eles') {
      setState(() {
        placarEles += pontos;

        if (placarEles >= 12) {
          vitoriaEles++;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (contexto) => const VitoriaPage(
                grupo: "Eles",
              ),
            ),
          ).then((acao) {
            if (acao == "desfazer") {
              desfazer();
            } else if (acao == "novaPartida") {
              novaPartida();
            }
          });
        }
      });
    }
    salvarDados();
  }

  void novaPartida() {
    setState(() {
      placarNos = 0;
      placarEles = 0;
    });
    salvarDados();
  }

  void novoTorneio() {
    setState(() {
      placarNos = 0;
      placarEles = 0;
      vitoriaNos = 0;
      vitoriaEles = 0;
    });
    salvarDados();
  }

  void desfazer() {
    if (grupoAtual == "Nos") {
      setState(() {
        placarNos -= pontoAtual;
        vitoriaNos--;
      });
    }
    if (grupoAtual == "Eles") {
      setState(() {
        placarEles -= pontoAtual;
        vitoriaEles--;
      });
    }
    salvarDados();
  }

  void diminuirPontos(String grupo) {
    if (grupo == 'Nos') {
      if (placarNos > 0) {
        setState(() {
          placarNos--;
        });
      }
    }

    if (grupo == 'Eles') {
      if (placarEles > 0) {
        setState(() {
          placarEles--;
        });
      }
    }
    salvarDados();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Contador de Truco"),
        centerTitle: true,
        backgroundColor: const Color(0xFF7D6B7D),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      const Text(
                        "Nós",
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF7D6B7D),
                        ),
                      ),
                      Container(
                        width: 120,
                        margin: const EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                            color: const Color(0xffffffdc),
                            border: Border.all(
                              color: const Color(0xFF7D6B7D),
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                          child: Text(
                            placarNos.toString(),
                            style: const TextStyle(
                              fontSize: 60,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFE22364),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          diminuirPontos('Nos');
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 20),
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: const Color(0xFF7D6B7D),
                            ),
                            color: const Color(0x0ffe5e5e),
                            shape: BoxShape.circle,
                          ),
                          child: const Text(
                            "-1",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF707070),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          aumentarPontos('Nos', 1);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF7D6B7D),
                          minimumSize: const Size(120, 60),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "+1",
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          aumentarPontos('Nos', 3);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF7D6B7D),
                          minimumSize: const Size(120, 60),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "+3",
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          aumentarPontos('Nos', 6);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF7D6B7D),
                          minimumSize: const Size(120, 60),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "+6",
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      const Text(
                        'Eles',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF7D6B7D),
                        ),
                      ),
                      Container(
                        width: 120,
                        margin: const EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                            color: const Color(0xffffffdc),
                            border: Border.all(
                              color: const Color(0xFF7D6B7D),
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                          child: Text(
                            placarEles.toString(),
                            style: const TextStyle(
                              fontSize: 60,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFE22364),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => diminuirPontos('Eles'),
                        child: Container(
                          margin: const EdgeInsets.only(top: 20),
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: const Color(0xFF7D6B7D),
                            ),
                            color: const Color(0x0ffe5e5e),
                            shape: BoxShape.circle,
                          ),
                          child: const Text(
                            "-1",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF707070),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          aumentarPontos('Eles', 1);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF7D6B7D),
                          minimumSize: const Size(120, 60),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "+1",
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          aumentarPontos('Eles', 3);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF7D6B7D),
                          minimumSize: const Size(120, 60),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "+3",
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          aumentarPontos('Eles', 6);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF7D6B7D),
                          minimumSize: const Size(120, 60),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "+6",
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 20,
                horizontal: 10,
              ),
              child: Image.asset("assets/banner-cartas.png"),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  vitoriaNos.toString(),
                  style: const TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Color(0xffe22364),
                  ),
                ),
                const Text(
                  'Vitórias',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff7d6b7d),
                  ),
                ),
                Text(
                  vitoriaEles.toString(),
                  style: const TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Color(0xffe22364),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        novaPartida();
                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.symmetric(
                            vertical: 15, horizontal: 10),
                        backgroundColor: const Color(0xff7d6b7d),
                        shape: const BeveledRectangleBorder(),
                      ),
                      child: const Text(
                        "Nova Partida",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        novoTorneio();
                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.symmetric(
                            vertical: 15, horizontal: 10),
                        backgroundColor: const Color(0x0ffe5e5e),
                        shape: const BeveledRectangleBorder(
                            side: BorderSide(color: Color(0xFF7D6B7D))),
                      ),
                      child: const Text(
                        "Novo Torneio",
                        style:
                            TextStyle(fontSize: 18, color: Color(0xFF7D6B7D)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
