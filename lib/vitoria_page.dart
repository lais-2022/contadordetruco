import 'package:flutter/material.dart';

class VitoriaPage extends StatelessWidget {
  final String grupo;

  const VitoriaPage({Key? key, required this.grupo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Vitoria'),
          centerTitle: true,
          backgroundColor: const Color(0xFF7D6B7D),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context, "desfazer");
            },
            child: const Icon(Icons.undo),
          ),
          ),

      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 40,
              ),
              const Text(
                "Parabéns!",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF707070),
                ),
              ),
              Text(
                "Vitória do Grupo $grupo",
                style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF707070),
                ),
              ),
              Image.asset('assets/trofeu.png'),
              const SizedBox(
                height: 70,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context, "novaPartida");
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xff7d6b7d),
                    ),
                    child: const Text(
                      'Nova Partida',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
